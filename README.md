# Tagging System

## What's needed

- Make sure you have [postgresql](https://www.postgresql.org/download/) installed on machine 
- Make sure you have [node.js](https://nodejs.org/en/download/) installed

## Database Connections - PostgreSQL

Create a database and set up credentials in .env as in .env.example

## Getting started

- Download the project’s zip
- After Creating Database, Check your connection and Create new tables in PostgreSQL

```
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255)
);

CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  user_id INTEGER REFERENCES users(id),
  product_name VARCHAR(255)
);

CREATE TABLE tags (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255),
  color VARCHAR(255)
);

CREATE TABLE user_tags (
  id SERIAL PRIMARY KEY,
  user_id INTEGER REFERENCES users(id),
  tag_id INTEGER REFERENCES tags(id)
);

CREATE TABLE order_tags (
  id SERIAL PRIMARY KEY,
  order_id INTEGER REFERENCES orders(id),
  tag_id INTEGER REFERENCES tags(id)
);

```

- Type `npm install` in terminal/console in the source folder where `package.json` is located and
- Type `npm run start` in terminal/console 


The following table shows overview of the Rest APIs that will be exported:

- POST    `api/users/1/tags`	      Add tag to user
- PUT     `api/users/1/tags/1`        Edit tag for user
- DELETE  `api/users/1/tags/1`        Delete tag for user
- GET     `api/users/tags/VIP`        Get all users with tag
- GET     `api/orders/tags/VIP`       Get all orders with tag:

## Testing the API

To test the API, we can use a tool like curl or Postman. Here are some examples of requests:

**Add a tag to a user:**

POST api/users/1/tags
{
"name": "VIP",
"color": "#ff0000"
}

**Edit a tag for a user:**

PUT api/users/1/tags/1
{
"name": "VIP",
"color": "#00ff00"
}

**Delete a tag for a user:**

DELETE api/users/1/tags/1

**Get all users with a tag:**

GET api/users/tags/VIP

**Get all orders with a tag:**

GET api/orders/tags/VIP




