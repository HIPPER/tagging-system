const express = require("express");
const taggingController = require("../controllers/taggingController");

const router = express.Router();

// get all users with a tag
router.get("/users/tags/:tagName", taggingController.getUsersWithTag);

// get all orders with a tag
router.get("/orders/tags/:tagName", taggingController.getOrdersWithTag);

// add a tag to a user
router.post("/users/:userId/tags", taggingController.createTagToUser);

// add a tag to an order
router.post("/orders/:orderId/tags", taggingController.createTagToOrder);

// edit a tag for a user
router.put("/users/:userId/tags/:tagId", taggingController.updateTagForUser);

// edit a tag for an order
router.put("/orders/:orderId/tags/:tagId", taggingController.updateTagForOrder);

// delete a tag for a user
router.delete("/users/:userId/tags/:tagId", taggingController.deleteTagForUser);

// delete a tag for an order
router.delete(
  "/orders/:orderId/tags/:tagId",
  taggingController.deleteTagForOrder
);

module.exports = router;
