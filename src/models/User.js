const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = require("./");

class User extends Model {}
User.init(
  {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
  },
  { sequelize, modelName: "user", timestamps: false }
);

module.exports = User;
