const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = require("./");
const User = require("./User");
const Order = require("./Order");

class Tag extends Model {}
Tag.init(
  {
    name: DataTypes.STRING,
    color: DataTypes.STRING,
  },
  { sequelize, modelName: "tag", timestamps: false }
);

Tag.belongsToMany(User, { through: "user_tags", foreignKey: "tag_id" });
Tag.belongsToMany(Order, { through: "order_tags", foreignKey: "tag_id" });

Order.belongsToMany(Tag, { through: "order_tags", foreignKey: "order_id" });
User.belongsToMany(Tag, { through: "user_tags", foreignKey: "user_id" });

module.exports = Tag;
