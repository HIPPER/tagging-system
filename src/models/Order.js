const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = require("./");

class Order extends Model {}
Order.init(
  {
    user_id: DataTypes.INTEGER,
    product_name: DataTypes.STRING,
  },
  { sequelize, modelName: "order", timestamps: false }
);

module.exports = Order;
