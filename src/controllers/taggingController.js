const taggingService = require("../services/taggingService");

const getUsersWithTag = async (req, res) => {
  const users = await taggingService.findUsersWithTag(req.params.tagName);
  res.send(users);
};

const getOrdersWithTag = async (req, res) => {
  const orders = await taggingService.findOrdersWithTag(req.params.tagName);
  res.send(orders);
};

const createTagToUser = async (req, res) => {
  const user = await taggingService.getUser(req.params.userId);
  if (!user) {
    return res.status(404).send("User not found");
  }

  const tag = await taggingService.createNewTag(req.body);
  await taggingService.addTagToUser(user, tag);
  res.send(tag);
};

const createTagToOrder = async (req, res) => {
  const order = await taggingService.getOrder(req.params.orderId);
  if (!order) {
    return res.status(404).send("Order not found");
  }

  const tag = await taggingService.createNewTag(req.body);
  await taggingService.addTagToOrder(order, tag);
  res.send(tag);
};

const updateTagForUser = async (req, res) => {
  const user = await taggingService.getUser(req.params.userId);
  if (!user) {
    return res.status(404).send("User not found");
  }
  const tag = await taggingService.getOneTag(req.params.tagId);
  if (!tag) {
    return res.status(404).send("Tag not found");
  }

  await taggingService.updateOneTag(tag, req.body);
  res.send("Update an existing tag");
};

const updateTagForOrder = async (req, res) => {
  const order = await taggingService.getOrder(req.params.orderId);
  if (!order) {
    return res.status(404).send("Order not found");
  }

  const tag = await taggingService.getOneTag(req.params.tagId);
  if (!tag) {
    return res.status(404).send("Tag not found");
  }

  await taggingService.updateOneTag(tag, req.body);
  res.send(tag);
};

const deleteTagForUser = async (req, res) => {
  const user = await taggingService.getUser(req.params.userId);
  if (!user) {
    return res.status(404).send("User not found");
  }
  const tag = await taggingService.getOneTag(req.params.tagId);
  if (!tag) {
    return res.status(404).send("Tag not found");
  }

  await taggingService.deleteTagForUser(user, tag);
  res.send("Tag removed");
};

const deleteTagForOrder = async (req, res) => {
  const order = await taggingService.getOrder(req.params.orderId);
  if (!order) {
    return res.status(404).send("Order not found");
  }
  const tag = await taggingService.getOneTag(req.params.tagId);
  if (!tag) {
    return res.status(404).send("Tag not found");
  }

  await taggingService.deleteTagForOrder(order, tag);
  res.send("Tag removed");
};

module.exports = {
  getUsersWithTag,
  getOrdersWithTag,
  createTagToUser,
  createTagToOrder,
  updateTagForUser,
  updateTagForOrder,
  deleteTagForUser,
  deleteTagForOrder,
};
