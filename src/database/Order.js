const Order = require("../models/Order");
const Tag = require("../models/Tag");

const getAllWithTag = async (tagName) => {
  const orders = await Order.findAll({
    include: [
      {
        model: Tag,
        where: { name: tagName },
      },
    ],
  });

  return orders;
};

const getOne = async (orderId) => {
  const order = await Order.findByPk(orderId);
  return order;
};

const addTag = async (order, tag) => {
  await order.addTag(tag);
};

const deleteTag = async (order, tag) => {
  await order.removeTag(tag);
};

module.exports = {
  getAllWithTag,
  getOne,
  addTag,
};
