const User = require("../models/User");
const Tag = require("../models/Tag");

const getAllWithTag = async (tagName) => {
  const users = await User.findAll({
    include: [
      {
        model: Tag,
        where: { name: tagName },
      },
    ],
  });

  return users;
};

const getUser = async (userId) => {
  const user = await User.findByPk(userId);
  return user;
};

const addTag = async (user, tag) => {
  await user.addTag(tag);
};

const deleteTag = async (user, tag) => {
  await user.removeTag(tag);
};

module.exports = {
  getAllWithTag,
  getUser,
  addTag,
  deleteTag,
};
