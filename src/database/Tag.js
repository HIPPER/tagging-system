const Tag = require("../models/Tag");

const create = async (body) => {
  const tag = await Tag.create(body);
  return tag;
};

const getOne = async (tagId) => {
  const tag = await Tag.findByPk(tagId);
  return tag;
};

const update = async (tag, body) => {
  const tagBody = await tag.update(body);
  return tagBody;
};

const remove = async (tagId) => {
  const tag = await Tag.findByPk(tagId);
  await tag.destroy();
};

module.exports = {
  create,
  getOne,
  update,
  remove,
};
