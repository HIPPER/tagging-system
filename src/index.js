require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");

const taggingRouter = require("./routes/taggingRoutes");
const db = require("./models");

const app = express();
const PORT = process.env.APP_PORT || 3000;

app.use(bodyParser.json());

db.sync()
  .then(() => {
    console.log("Database connected successfully!");
  })
  .catch((err) => {
    console.log("Failed to connect db: " + err.message);
  });

app.use("/api/", taggingRouter);

app.listen(PORT, () => {
  console.log(`API is listening on port ${PORT}`);
});
