const User = require("../database/User");
const Tag = require("../database/Tag");
const Order = require("../database/Order");

const findUsersWithTag = async (tagName) => {
  const users = await User.getAllWithTag(tagName);
  return users;
};

const findOrdersWithTag = async (tagName) => {
  const orders = await Order.getAllWithTag(tagName);
  return orders;
};

const getOneTag = async (tagId) => {
  const tag = await Tag.getOne(tagId);
  return tag;
};

const getUser = async (userId) => {
  const user = await User.getUser(userId);
  return user;
};

const getOrder = async (orderId) => {
  const order = await Order.getOne(orderId);
  return order;
};

const createNewTag = async (body) => {
  const tag = await Tag.create(body);
  return tag;
};

const addTagToUser = async (user, tag) => {
  await User.addTag(user, tag);
  return;
};

const addTagToOrder = async (order, tag) => {
  await Order.addTag(order, tag);
  return;
};

const updateOneTag = async (tag, body) => {
  const tagBody = await Tag.update(tag, body);
  return tagBody;
};

const deleteTag = async (tagId) => {
  await Tag.remove(tagId);
};

const deleteTagForUser = async (user, tag) => {
  await User.deleteTag(user, tag);
  return;
};

const deleteTagForOrder = async (order, tag) => {
  await Order.deleteTag(order, tag);
  return;
};

module.exports = {
  findUsersWithTag,
  findOrdersWithTag,
  getOneTag,
  getUser,
  getOrder,
  addTagToUser,
  addTagToOrder,
  createNewTag,
  updateOneTag,
  deleteTag,
  deleteTagForUser,
  deleteTagForOrder,
};
