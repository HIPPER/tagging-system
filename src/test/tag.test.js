const chai = require("chai");
const expect = chai.expect;
const { Pool } = require("pg");
const taggingService = require("../services/taggingService");
const app = require("../index");

const pool = new Pool({
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
});

describe("Tagging system", () => {
  describe("Tags", () => {
    before(async () => {
      await pool.query("DELETE FROM user_tags");
      await pool.query("DELETE FROM order_tags");
      await pool.query("DELETE FROM orders");
      await pool.query("DELETE FROM users");
      await pool.query("DELETE FROM tags");
    });

    after(async () => {
      await pool.query("DELETE FROM user_tags");
      await pool.query("DELETE FROM order_tags");
      await pool.query("DELETE FROM orders");
      await pool.query("DELETE FROM users");
      await pool.query("DELETE FROM tags");
    });

    describe("Create tag", () => {
      it("should create a new tag", async () => {
        const tag = { name: "Priority", color: "#ff0000" };
        const result = await taggingService.createNewTag(tag);
        expect(result).to.be.an("object");
        expect(result.name).to.equal(tag.name);
        expect(result.color).to.equal(tag.color);
      });
    });

    describe("Update tag", () => {
      let tag;

      before(async () => {
        const tagBody = { name: "Priority", color: "#ff0000" };
        const result = await taggingService.createNewTag(tagBody);
        tag = result;
      });

      it("should update an existing tag", async () => {
        const tagBody = { name: "Urgent", color: "#ff9900" };
        const response = await taggingService.updateOneTag(tag, tagBody);
        const result = response.dataValues;
        expect(result).to.be.an("object");
        expect(result.id).to.equal(tag.id);
        expect(result.name).to.equal(tagBody.name);
        expect(result.color).to.equal(tagBody.color);
      });
    });

    describe("Delete tag", () => {
      let tagId;

      before(async () => {
        const tag = { name: "Priority", color: "#ff0000" };
        const response = await taggingService.createNewTag(tag);
        const result = response.dataValues;
        tagId = result.id;
      });

      it("should delete an existing tag", async () => {
        await taggingService.deleteTag(tagId);
        const result = await pool.query(
          `SELECT * FROM tags WHERE id = ${tagId}`
        );
        expect(result.rows).to.have.lengthOf(0);
      });
    });
  });

  describe("Search users by tags", () => {
    let tagName1, tagName2;

    before(async () => {
      const data_tag1 = { name: "Priority", color: "#ff0000" };
      const data_tag2 = { name: "Urgent", color: "#ff9900" };

      const tag1 = await taggingService.createNewTag(data_tag1);
      const tag2 = await taggingService.createNewTag(data_tag2);

      tagName1 = tag1.dataValues.name;
      tagName2 = tag2.dataValues.name;

      const data_user1 = {
        name: "John Doe",
        email: "john.doe@example.com",
      };
      const data_user2 = {
        name: "Jane Doe",
        email: "jane.doe@example.com",
      };

      const result3 = await pool.query(
        `INSERT INTO users (name, email) VALUES ('${data_user1.name}', '${data_user1.email}') RETURNING id`
      );
      const result4 = await pool.query(
        `INSERT INTO users (name, email) VALUES ('${data_user2.name}', '${data_user2.email}') RETURNING id`
      );

      const userId1 = result3.rows[0].id;
      const userId2 = result4.rows[0].id;

      const user1 = await taggingService.getUser(userId1);
      const user2 = await taggingService.getUser(userId2);

      await taggingService.addTagToUser(user1, tag1);
      await taggingService.addTagToUser(user2, tag2);
    });

    it("should return users with matching tags", async () => {
      const response = await taggingService.findUsersWithTag(tagName1);
      const result = response[0].dataValues;
      expect(response).to.be.an("array");
      expect(response).to.have.lengthOf(1);
      expect(result).to.have.property("name", "John Doe");
      expect(result).to.have.property("email", "john.doe@example.com");
    });
  });

  describe("Search orders by tags", () => {
    let tagName, userId;

    before(async () => {
      const data_tag = { name: "Priority", color: "#ff0000" };
      const tag1 = await taggingService.createNewTag(data_tag);
      tagName = tag1.dataValues.name;

      const user1 = {
        name: "John Doe",
        email: "john.doe@example.com",
      };

      const result = await pool.query(
        `INSERT INTO users (name, email) VALUES ('${user1.name}', '${user1.email}') RETURNING id`
      );
      userId = result.rows[0].id;

      const orderId1 = await pool.query(
        `INSERT INTO orders (user_id, product_name) VALUES ('${userId}', 'tea') RETURNING id`
      );
      const orderId2 = await pool.query(
        `INSERT INTO orders (user_id, product_name) VALUES ('${userId}', 'coffee') RETURNING id`
      );

      const order1 = await taggingService.getOrder(orderId1.rows[0].id);
      const order2 = await taggingService.getOrder(orderId2.rows[0].id);

      await taggingService.addTagToOrder(order1, tag1);
      await taggingService.addTagToOrder(order2, tag1);
    });

    it("should return orders with matching tags", async () => {
      const result = await taggingService.findOrdersWithTag(tagName);
      expect(result).to.be.an("array");
      expect(result).to.have.lengthOf(2);
      expect(result[0].dataValues).to.have.property("user_id", userId);
      expect(result[0].dataValues).to.have.property("product_name", "tea");
    });
  });
});
